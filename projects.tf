resource "gitlab_project" "docs" {
  name                   = "docs"
  description            = "Overview e documentação do grupo 'GitOps Training' e seus subprojetos."
  default_branch         = "main"
  namespace_id           = data.gitlab_group.gitops-training.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "aws" {
  name                   = "aws"
  description            = "Provisionamento de infraestrutura na cloud da AWS para aplicações do grupo 'apps'."
  default_branch         = "main"
  namespace_id           = gitlab_group.infrastructure.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

resource "gitlab_project" "cluster-management" {
  name                   = "cluster-management"
  description            = "Gerenciamento de aplicações no cluster kubernetes com Gitlab."
  default_branch         = "main"
  namespace_id           = gitlab_group.apps.id
  shared_runners_enabled = "true"
  visibility_level       = "public"
}

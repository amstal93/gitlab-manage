data "gitlab_group" "gitops-training" {
  full_path = "gitops-training"
}

resource "gitlab_group" "apps" {
  name                = "apps"
  description         = "Aplicações para auto deploy."
  path                = "apps"
  parent_id           = data.gitlab_group.gitops-training.id
  visibility_level    = "public"
  auto_devops_enabled = "true"
  emails_disabled     = "false"
}

resource "gitlab_group" "infrastructure" {
  name                = "infrastructure"
  description         = "Infraestrutura em código para diferentes clouds."
  path                = "infrastructure"
  parent_id           = data.gitlab_group.gitops-training.id
  visibility_level    = "public"
  auto_devops_enabled = "false"
}
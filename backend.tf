terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "gitops-training"

    workspaces {
      name = "gitlab-manage"
    }
  }
}
